import os
from app import create_app
from gevent.pywsgi import WSGIServer


app = create_app(os.getenv('APP_ENV') or 'default')
debug = app.config["DEBUG"]

def select_port():
    port = app.config["PORT"]
    if not port:
        port = 5000
        print("!!! app port not set running on default port !!! to specify port please run 'export port=<port>'")
    return int(port)


port = select_port()

if __name__ == '__main__':
    print("############### Getting server up and running ################")
    if os.getenv('APP_ENV') == 'production':
        print("Running prouction server on port {0}".format(port))
        http_server = WSGIServer(('0.0.0.0', port), app, None, 'default')
        http_server.serve_forever()
    else:
        print("Running development server on port {0}".format(port))
        http_server = WSGIServer(('0.0.0.0', port), app, None, 'default')
        http_server.serve_forever()
