from . import api
from flask import request,Response
import json

from models import make_request_to_compliance


@api.route('/post_permit_id',methods=["POST"])
def post_permit_id():
    """
    This method exxtracts the data from the post request
    Expecting json format data
    {
    "permit_id": "mhhgfb657iny78t5yygyt"
    }
    :return:
    """
    json_data = request.get_json()

    if type(json_data)==dict and json_data.get("permit_id"):
        permit_id = json_data.get("permit_id")
        get_req_rsp = make_request_to_compliance(permit_id)
        if get_req_rsp[0]:
            data = {"msg": get_req_rsp[2],"data":get_req_rsp[3]}
            status = get_req_rsp[1]
            return Response(status=status, mimetype="application/json", response=json.dumps(data))


        else:
            data = {"msg": get_req_rsp[2]}
            status = get_req_rsp[1]
            return Response(status=status, mimetype="application/json", response=json.dumps(data))

    else:
        data = {"msg": "permit_id key not found in the post data"}
        status = 400
        return Response(status=status,mimetype="application/json",response=json.dumps(data))





