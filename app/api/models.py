import requests

# we will change this
base_compliacnce_service_url = "https://services.tpsd.create.farm/tps/compliance/cola/permit/"


def make_request_to_compliance(permit_id):
    """
    This method makes request to the complicance sevice
    :return:
    """
    url = base_compliacnce_service_url + str(permit_id)
    try:
        resp = requests.get(url=url)
    except Exception as e:
        status = 400
        msg = "Error reaching compliance service"
        return False, status, msg

    status = resp.status_code
    msg = "request sucess"
    data = resp.text

    return True,status,msg,data

