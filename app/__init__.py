from flask import Flask
from config import config
import os

app_config = config[os.getenv('APP_ENV') or 'default']


def create_app(configuration):

    app = Flask(__name__)
    app.config.from_object(config[configuration])
    config[configuration].init_app(app)

    from .api import api as api_blueprint

    app.register_blueprint(api_blueprint, url_prefix='/api')
    return app
