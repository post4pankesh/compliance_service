import os


class Config:
    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    PORT = os.getenv("port",5000)

class LocalConfig(Config):
    DEBUG = True
    PORT = os.getenv("port",5000)


class ProductionConfig(Config):
    DEBUG = True
    PORT = os.getenv("port",5000)

class TestingConfig(Config):
    DEBUG = True

config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': LocalConfig,
    'testing':TestingConfig
    }
